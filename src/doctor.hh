#ifndef _DOCTOR_HH_
#define _DOCTOR_HH_

#include <string>
#include <vector>

#include "nlohmann/json.hpp"

using json = nlohmann::json;

namespace doctor
{
    void check_duplicate_names(json &j) noexcept;
    void check_duplicate_paths(json &j) noexcept;
    void check_duplicate_sets(json &j) noexcept;
    void check_json_schema(json &j) noexcept;
    void check_paths_exist(json &j) noexcept;
}

namespace cmd
{
    void doctor(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _DOCTOR_HH_
