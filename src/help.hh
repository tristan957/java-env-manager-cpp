#ifndef _HELP_HH_
#define _HELP_HH_

#include <string>
#include <vector>

namespace cmd
{
    void help(std::vector<std::string> args, bool debug = false) noexcept;
}

namespace help
{
    void add() noexcept;
    void doctor() noexcept;
    void help() noexcept;
    void init() noexcept;
    void list() noexcept;
    void remove() noexcept;
    void set() noexcept;
    void update() noexcept;
    void version() noexcept;
    void which() noexcept;
}

#endif    // _HELP_HH_
