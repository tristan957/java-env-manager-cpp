#include <experimental/filesystem>
#include <fstream>
#include <string>
#include <vector>

#include "init.hh"
#include "nlohmann/json.hpp"
#include "utility.hh"

namespace fs = std::experimental::filesystem;
using json   = nlohmann::json;

void cmd::init(std::vector<std::string> args, bool debug) noexcept
{
    const std::string program_dir = util::get_program_dir();
    fs::create_directory(program_dir);

    auto file    = std::ofstream(program_dir + SETTINGS_FILE);
    const auto j = json({{"distributions", json::array()}});
    file << std::setw(4) << j << std::endl;
    file.close();
}
