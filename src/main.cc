#include <functional>
#include <getopt.h>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "add.hh"
#include "doctor.hh"
#include "help.hh"
#include "init.hh"
#include "list.hh"
#include "remove.hh"
#include "set.hh"
#include "update.hh"
#include "utility.hh"
#include "version.hh"
#include "which.hh"

int main(const int argc, char *const *argv)
{
    if (argc < 2) {
        cmd::help(std::vector<std::string>());
        return 1;
    }

    // Whether to make log statements
    bool DEBUG = false;

    // Represents number of args before a subcommand (1 for program name)
    int num_precommand_args  = 1;
    const char *short_opts   = "dv";
    const option long_opts[] = {
        {"debug", 0, nullptr, 'd'}, {"version", 0, nullptr, 'v'}, {nullptr, 0, nullptr, 0}};
    while (true) {
        const int opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);
        if (opt == -1) {
            break;
        }

        num_precommand_args++;
        switch (opt) {
        case 'd':
            DEBUG = true;
            break;
        case 'v':
            std::cout << "Java Environment Manager\n"
                      << "Version: " << VERSION << '\n'
                      << "License: MIT\n";
            return 0;
        default:
            std::cerr << '-' << opt << " is not a valid option\n";
            cmd::help(std::vector<std::string>(), DEBUG);
            return 1;
        }
    }

    // create subcommand argument array
    auto command_args = std::vector<std::string>();
    command_args.assign(argv + num_precommand_args, argv + argc);

    // mapping subcommands to their functions
    const std::map<std::string, std::function<void(std::vector<std::string>, bool)>> sub_commands =
        {{"add", cmd::add},        {"doctor", cmd::doctor}, {"help", cmd::help},
         {"init", cmd::init},      {"list", cmd::list},     {"remove", cmd::remove},
         {"set", cmd::set},        {"which", cmd::which},   {"update", cmd::update},
         {"version", cmd::version}};

    // find and run subcommand
    auto p = sub_commands.find(argv[1]);
    if (p != sub_commands.end()) {
        p->second(command_args, DEBUG);
    } else {
        std::cout << argv[1] << " not a valid subcommand\n";
        cmd::help(std::vector<std::string>(), DEBUG);
        return 1;
    }

    return 0;
}
