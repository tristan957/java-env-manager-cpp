#include <iostream>
#include <string>
#include <vector>

#include "nlohmann/json.hpp"
#include "remove.hh"
#include "utility.hh"

using json = nlohmann::json;

void cmd::remove(std::vector<std::string> args, bool debug) noexcept
{
    if (args.size() < 2) {
        std::cout << "Not enough arguments\n";
        exit(1);
    }

    json j = util::get_settings();

    for (size_t i = 1; i < args.size(); i++) {
        bool name_found = false;
        for (size_t k = 0; k < j["distributions"].size(); k++) {
            const json &distro = j["distributions"][k];
            if (distro["name"].get<std::string>().compare(args[1]) == 0) {
                name_found = true;
                j["distributions"].erase(k);
            }
        }

        if (!name_found) {
            std::cout << args[i] << " not found in " << SETTINGS_FILE << '\n';
            exit(1);
        }
    }

    util::set_settings(j);
}
