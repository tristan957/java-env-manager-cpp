#include <algorithm>
#include <iterator>
#include <sstream>
#include <string>

#include "nlohmann/json.hpp"

#define FOLDER_NAME "/.java-env-manager/"    // default folder name
#define LOG_FILE "log.out"                   // name of log file
#define SETTINGS_FILE "settings.json"        // name of settings file
#define VERSION "0.6.0"                      // version of Java Environment Manager

using json = nlohmann::json;

namespace util
{
    const std::array<std::string, 3> KEYS = {"name", "path", "set"};

    template<class Container>
    std::string container_2_string(Container duplicate_set)
    {
        auto ss = std::stringstream();
        std::copy(duplicate_set.begin(), duplicate_set.end(),
                  std::ostream_iterator<std::string>(ss, ", "));
        auto dupe_string = ss.str();
        return dupe_string.substr(0, dupe_string.length() - 2);
    }

    /*
     * Returns the Java Environment Manager home directory
     */
    const std::string get_program_dir() noexcept;

    /*
     * Returns the Java Environment Manager bin directory
     */
    const std::string get_program_bin_dir(std::string program_dir = std::string()) noexcept;

    /*
     * Returns the JSON representation of the settings file
     */
    json get_settings(std::string program_dir = std::string());

    /*
     * Sets the settings using a JSON object
     */
    void set_settings(json &j, std::string program_dir = std::string()) noexcept;
}
