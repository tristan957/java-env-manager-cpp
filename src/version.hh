#ifndef _VERSION_HH_
#define _VERSION_HH_

#include <string>
#include <vector>

namespace cmd
{
    void version(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _VERSION_HH_
